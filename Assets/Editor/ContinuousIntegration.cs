using System.Linq;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;

public static class ContinuousIntegration
{
    /// <summary>
    /// ������ ����� ��� Windows ����� ��������� ������
    /// </summary>
    public static void BuildWin()
    {
        Build();


        //d
    }

    /// <summary>
    /// ������ ����� ��� ����������� ���������
    /// </summary>
    /// <param name="target">���������</param>
    /// <param name="outPath">���� ��� �����</param>
    public static void Build()
    {
        var buildPlayerOptions = new BuildPlayerOptions
        {
            target = BuildTarget.StandaloneWindows64,
            //�������� ��� ����� ����������� �������� � ����
            scenes = (from settingsScene in EditorBuildSettings.scenes
                      where settingsScene.enabled
                      select settingsScene.path).ToArray(),
            locationPathName = "Build/123.exe"
        };
        BuildReport buildReport =  BuildPipeline.BuildPlayer(buildPlayerOptions);

        
        BuildSummary summary = buildReport.summary;

        if (summary.result == BuildResult.Succeeded)
        {
            Debug.Log("Build succeeded: " + summary.totalSize + " bytes" + "     " + summary.outputPath);

        }
        if (summary.result == BuildResult.Failed)
        {
            Debug.Log("Build failed");
        }
    }
}